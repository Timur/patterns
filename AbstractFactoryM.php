<?php
abstract class CommsManager
{
    CONST APPT = 1;
    CONST TTD = 2;
    CONST CONTACT = 3;

    abstract public function getHeaderText();
    abstract public function make($flag);
    abstract public function getFooterText();
}

interface ApptEncoder
{
    public function encode();
}

interface TtdEncoder
{
    public function encode();
}

interface ContactEncoder
{
    public function encode();
}

class MegaApptEncoder implements ApptEncoder
{
    public function encode()
    {
        return "Встеча закодирована ".__CLASS__;
    }
}

class BloggsApptEncoder implements ApptEncoder
{
    public function encode()
    {
        return "Встеча закодирована ".__CLASS__;
    }
}

class MegaTtdEncoder implements TtdEncoder
{
    public function encode()
    {
        return "Задача закодирована ".__CLASS__;
    }
}

class BloggsTtdEncoder implements TtdEncoder
{
    public function encode()
    {
        return "Задача закодирована ".__CLASS__;
    }
}

class MegaContactEncoder implements ContactEncoder
{
    public function encode()
    {
        return "Контакт закодирован ".__CLASS__;
    }
}

class BloggsContactEncoder implements ContactEncoder
{
    public function encode()
    {
        return "Контакт закодирован ".__CLASS__;
    }
}

class BloggsCommsManager extends CommsManager
{
    public function getHeaderText()
    {
        return "BloggsCall верхний колонтитул";
    }

    public function make($flag)
    {
        switch ($flag){
            case self::APPT:
                return new BloggsApptEncoder();
            case self::CONTACT:
                return new BloggsContactEncoder();
            case self::TTD:
                return new BloggsTtdEncoder();
        }
    }

    public function getFooterText()
    {
        return "BloggsCall нижний колонтитул";
    }
}

$creator = new BloggsCommsManager();
echo $creator->make(BloggsCommsManager::APPT)->encode(); //Встеча закодирована BloggsApptEncoder